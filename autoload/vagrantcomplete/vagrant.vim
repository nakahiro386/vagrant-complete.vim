"=============================================================================
" File: autoload/vagrantcomplete/vagrant.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

let s:commands = [
  \ { 'word': 'box', 'menu': 'manages boxes: installation, removal, etc.' },
  \ { 'word': 'cap', 'menu': 'checks and executes capability' },
  \ { 'word': 'connect', 'menu': 'connect to a remotely shared Vagrant environment' },
  \ { 'word': 'destroy', 'menu': 'stops and deletes all traces of the vagrant machine' },
  \ { 'word': 'docker-logs', 'menu': 'outputs the logs from the Docker container' },
  \ { 'word': 'docker-run', 'menu': 'run a one-off command in the context of a container' },
  \ { 'word': 'exec', 'menu': 'executes commands in virtual machine' },
  \ { 'word': 'global-status', 'menu': 'outputs status Vagrant environments for this user' },
  \ { 'word': 'halt', 'menu': 'stops the vagrant machine' },
  \ { 'word': 'help', 'menu': 'shows the help for a subcommand' },
  \ { 'word': 'hostmanager', 'menu': 'plugin: vagrant-hostmanager: manages the /etc/hosts file within a multi-machine environment' },
  \ { 'word': 'init', 'menu': 'initializes a new Vagrant environment by creating a Vagrantfile' },
  \ { 'word': 'list-commands', 'menu': 'outputs all available Vagrant subcommands, even non-primary ones' },
  \ { 'word': 'login', 'menu': "log in to HashiCorp's Atlas" },
  \ { 'word': 'package', 'menu': 'packages a running vagrant environment into a box' },
  \ { 'word': 'plugin', 'menu': 'manages plugins: install, uninstall, update, etc.' },
  \ { 'word': 'port', 'menu': 'displays information about guest port mappings' },
  \ { 'word': 'powershell', 'menu': 'connects to machine via powershell remoting' },
  \ { 'word': 'provider', 'menu': 'show provider for this environment' },
  \ { 'word': 'provision', 'menu': 'provisions the vagrant machine' },
  \ { 'word': 'push', 'menu': 'deploys code in this environment to a configured destination' },
  \ { 'word': 'rdp', 'menu': 'connects to machine via RDP' },
  \ { 'word': 'reload', 'menu': 'restarts vagrant machine, loads new Vagrantfile configuration' },
  \ { 'word': 'resume', 'menu': 'resume a suspended vagrant machine' },
  \ { 'word': 'rsync', 'menu': 'syncs rsync synced folders to remote machine' },
  \ { 'word': 'rsync-auto', 'menu': 'syncs rsync synced folders automatically when files change' },
  \ { 'word': 'share', 'menu': 'share your Vagrant environment with anyone in the world' },
  \ { 'word': 'snapshot', 'menu': 'manages snapshots: saving, restoring, etc.' },
  \ { 'word': 'ssh', 'menu': 'connects to machine via SSH' },
  \ { 'word': 'ssh-config', 'menu': 'outputs OpenSSH valid configuration to connect to the machine' },
  \ { 'word': 'status', 'menu': 'outputs status of the vagrant machine' },
  \ { 'word': 'suspend', 'menu': 'suspends the machine' },
  \ { 'word': 'teraterm', 'menu': 'connects to machine via SSH using TeraTerm' },
  \ { 'word': 'up', 'menu': 'starts and provisions the vagrant environment' },
  \ { 'word': 'vbguest', 'menu': '' },
  \ { 'word': 'version', 'menu': 'prints current and latest Vagrant version' },
  \ ]
let s:short_options = vagrantcomplete#utils#to_comp(['-v', '-h'])
let s:options =  vagrantcomplete#utils#to_comp(['--version', '--help'])

function! vagrantcomplete#vagrant#complete(arglead, cmdline)
  if a:arglead =~# '^-'
    return copy(s:short_options) + copy(s:options)
  elseif a:arglead =~# '^--'
    return copy(s:options)
  else
    return copy(s:commands) + copy(s:short_options) + copy(s:options)
  endif
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
