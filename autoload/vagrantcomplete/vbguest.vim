"=============================================================================
" File: autoload/vagrantcomplete/vbguest.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

let s:options = [
  \ { 'word': '--do', 'menu': '--do COMMAND Manually `start`, `rebuild` or `install` GuestAdditions.' },
  \ { 'word': '--status', 'menu': 'Print current GuestAdditions status and exit.' },
  \ { 'word': '--force', 'menu': 'Whether to force the installation. (Implied by --do start|rebuild|install)' },
  \ { 'word': '--auto-reboot', 'menu': "Allow rebooting the VM after installation. (when GuestAdditions won't start)" },
  \ { 'word': '--no-remote', 'menu': 'Do not attempt do download the iso file from a webserver' },
  \ { 'word': '--iso', 'menu': '--iso file_or_uri Full path or URI to the VBoxGuestAdditions.iso' },
  \ { 'word': '--no-cleanup', 'menu': 'Do not run cleanup tasks after installation. (for debugging)' },
  \ { 'word': '--provision', 'menu': 'Enable provisioning' },
  \ { 'word': '--no-provision', 'menu': 'Disable provisioning' },
  \ { 'word': '--provision-with', 'menu': '--provision-with x,y,z Enable only certain provisioners, by type or by name.' },
  \ { 'word': '--help', 'menu': 'Print help' },
  \ ]
let s:do_commands = vagrantcomplete#utils#to_comp([
  \ 'start',
  \ 'rebuild',
  \ 'install',
  \ ])
let s:short_options = [
  \ { 'word': '-f', 'menu': 'Whether to force the installation. (Implied by --do start|rebuild|install)' },
  \ { 'word': '-b', 'menu': "Allow rebooting the VM after installation. (when GuestAdditions won't start)" },
  \ { 'word': '-R', 'menu': 'Do not attempt do download the iso file from a webserver' },
  \ { 'word': '-h', 'menu': 'Print help' },
  \ ]

function! vagrantcomplete#vbguest#complete(arglead, cmdline)
  if  len(a:cmdline) > 2 && a:cmdline[-2] == '--do' && a:cmdline[-1] !~? '^-'
    return copy(s:do_commands)
  elseif a:arglead =~# '^--'
    return copy(s:options)
  else
    return copy(s:options) + copy(s:short_options) + vagrantcomplete#utils#get_vm_names()
  endif
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
