"=============================================================================
" File: autoload/vagrantcomplete/global_status.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

let s:options = [
  \ { 'word': '--prune', 'menu': 'Prune invalid entries.' },
  \ { 'word': '--help', 'menu': 'Print help' },
  \ { 'word': '-h', 'menu': 'Print help' },
  \ ]

function! vagrantcomplete#global_status#complete(arglead, cmdline)
  return copy(s:options)
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
