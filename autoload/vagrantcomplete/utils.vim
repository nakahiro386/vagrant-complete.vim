"=============================================================================
" File: autoload/vagrantcomplete/utils.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

let s:providers = [
  \ 'virtualbox',
  \ 'vmware_fusion',
  \ 'vmware_workstation',
  \ 'docker',
  \ 'hyperv',
  \ ]

function! vagrantcomplete#utils#to_comp(list)
  return map(a:list, "{ 'word': v:val, 'menu': 'vagrant' }")
endfunction

function! vagrantcomplete#utils#get_providers()
  return copy(s:providers)
endfunction

function! vagrantcomplete#utils#get_vm_names()
  if get(g:, 'vagrantcomplete#strict_mode', 0)
    return s:get_vm_names_from_status()
  else
    return s:get_vm_names_from_dir()
  endif
endfunction

function! s:get_vm_names_from_dir()
  let l:vm_dir = finddir('.vagrant/machines/', '.;')
  if empty(l:vm_dir)
    return []
  endif
  let l:vm_names = map(globpath(l:vm_dir, '*', 0, 1),
    \ 'fnamemodify(v:val, ":t")')
  return vagrantcomplete#utils#to_comp(l:vm_names)
endfunction

function! s:get_vm_names_from_status()
  silent let l:status = systemlist('vagrant status --machine-readable')
  if v:shell_error || empty(l:status)
    return []
  endif
  call filter(map(l:status, 'split(v:val, ",", )'), 'v:val[2] ==# "state"')
  call map(l:status, '{ "word": v:val[1], "menu": v:val[3] }')
  return l:status
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
