"=============================================================================
" File: autoload/vagrantcomplete/up.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

let s:options = [
  \ { 'word': '--provision', 'menu': 'Enable provisioning' },
  \ { 'word': '--no-provision', 'menu': 'Disable provisioning' },
  \ { 'word': '--provision-with', 'menu': 'Enable only certain provisioners, by type or by name.' },
  \ { 'word': '--destroy-on-error', 'menu': 'Destroy machine if any fatal error happens (default to true)' },
  \ { 'word': '--no-destroy-on-error', 'menu': 'Destroy machine if any fatal error happens (default to true)' },
  \ { 'word': '--parallel', 'menu': 'Enable parallelism if provider supports it' },
  \ { 'word': '--no-parallel', 'menu': 'Disable parallelism if provider supports it' },
  \ { 'word': '--provider', 'menu': '--provider PROVIDER Back the machine with a specific provider' },
  \ { 'word': '--install-provider', 'menu': "If possible, install the provider if it isn't installed" },
  \ { 'word': '--no-install-provider', 'menu': "If possible, install the provider if it isn't installed" },
  \ { 'word': '--help', 'menu': 'Print help' },
  \ { 'word': '-h', 'menu': 'Print help' },
  \ ]

function! vagrantcomplete#up#complete(arglead, cmdline)
  if  len(a:cmdline) > 2 && a:cmdline[-2] == '--provider' && a:cmdline[-1] !~? '^-'
    return vagrantcomplete#utils#to_comp(vagrantcomplete#utils#get_providers())
  endif
  return vagrantcomplete#utils#get_vm_names() + copy(s:options)
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
