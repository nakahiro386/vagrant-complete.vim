"=============================================================================
" File: autoload/vagrantcomplete/provider.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

let s:commands = vagrantcomplete#utils#to_comp(
  \   vagrantcomplete#utils#get_providers()
  \ )

let s:options = [
  \ { 'word': '--install', 'menu': 'Installs the provider if possible' },
  \ { 'word': '--usable', 'menu': 'Checks if the named provider is usable' },
  \ { 'word': '--help', 'menu': 'Print help' },
  \ { 'word': '-h', 'menu': 'Print help' },
  \ ]

function! vagrantcomplete#provider#complete(arglead, cmdline)
  if a:arglead =~# '^-'
    return copy(s:options)
  else
    return copy(s:options) + copy(s:commands)
  endif
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
