"=============================================================================
" File: autoload/vagrantcomplete.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

function! vagrantcomplete#complete(arglead, cmdline)
  let l:command = s:get_command(a:cmdline)
  if l:command ==# ''
    return vagrantcomplete#vagrant#complete(a:arglead, a:cmdline)
  else
    let l:command = substitute(l:command, '-', '_', 'g')
    try
      return call('vagrantcomplete#' . l:command . '#complete',
        \ [a:arglead, a:cmdline])
    catch /^Vim\%((\a\+)\)\=:E117/
      return vagrantcomplete#utils#get_vm_names()
    endtry
  endif
endfunction

function! s:get_command(cmdline)
  let i = 0
  let len = len(a:cmdline) - 1
  while i < len
    let arg = a:cmdline[i]
    if arg !~# '^-'
      return arg
    endif
    let i = i + 1
  endwhile
  return ''
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
