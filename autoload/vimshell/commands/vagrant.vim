"=============================================================================
" File: autoload/vimshell/commands/vagrant.vim
" Author: nakahiro386
" Created: 2016-04-05
"=============================================================================

let s:save_cpo = &cpo
set cpo&vim

let s:command = {
  \   'name': 'vagrant',
  \   'kind': 'internal',
  \   'description': 'vagrant [options] <command> [<args>]'
  \ }

function! vimshell#commands#vagrant#define()
  return s:command
endfunction

function! s:command.complete(args)
  let l:arglead = a:args[-1]
  let l:cmdline = a:args
  return vimshell#complete#helper#keyword_filter(
    \ vagrantcomplete#complete(l:arglead, l:cmdline), l:arglead)
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
