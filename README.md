## vagrant-complete.vim

* Functions
  * vagrantcomplete#complete({arglead}, {cmdline})


## installation
```vim
NeoBundle 'bitbucket:nakahiro386/vagrant-complete.vim' {
  \   'type': 'git'
  \ }
```

